package ca.montreal.api.transport.entities;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result{
    @JsonProperty("records") 
    public ArrayList<RecordData> records;
    public ArrayList<Field> fields;
    public String sql;
}
