package ca.montreal.api.transport.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RecordData{
    @JsonProperty("WBT") 
    public String wBT;
    @JsonProperty("WBRT") 
    public String wBRT;
    @JsonProperty("Approche_Nord") 
    public String approche_Nord;
    @JsonProperty("Approche_Est") 
    public String approche_Est;
    @JsonProperty("Nom_Intersection") 
    public String nom_Intersection;
    @JsonProperty("Latitude") 
    public String latitude;
    @JsonProperty("Id_Intersection") 
    public String id_Intersection;
    @JsonProperty("NBRT") 
    public String nBRT;
    @JsonProperty("NBT") 
    public String nBT;
    @JsonProperty("Localisation_X") 
    public String localisation_X;
    @JsonProperty("Code_Banque") 
    public String code_Banque;
    @JsonProperty("Date") 
    public Date date;
    @JsonProperty("SBRT") 
    public String sBRT;
    @JsonProperty("Heure") 
    public String heure;
    @JsonProperty("Seconde") 
    public String seconde;
    @JsonProperty("NBLT") 
    public String nBLT;
    @JsonProperty("Longitude") 
    public String longitude;
    @JsonProperty("EBRT") 
    public String eBRT;
    @JsonProperty("EBLT") 
    public String eBLT;
    @JsonProperty("Localisation_Y") 
    public String localisation_Y;
    @JsonProperty("Approche_Sud") 
    public String approche_Sud;
    @JsonProperty("WBLT") 
    public String wBLT;
    @JsonProperty("Approche_Ouest") 
    public String approche_Ouest;
    @JsonProperty("Periode") 
    public String periode;
    @JsonProperty("SBT") 
    public String sBT;
    @JsonProperty("SBLT") 
    public String sBLT;
    @JsonProperty("Description_Code_Banque") 
    public String description_Code_Banque;
    public String _full_text;
    @JsonProperty("Id_Reference") 
    public String id_Reference;
    @JsonProperty("EBT") 
    public String eBT;
    @JsonProperty("Minute") 
    public String minute;
    public int _id;
}