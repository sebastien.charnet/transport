package ca.montreal.api.transport.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Field{
    public String type;
    public String id;
}
