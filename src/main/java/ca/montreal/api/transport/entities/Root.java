package ca.montreal.api.transport.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Root{
    public String help;
    public boolean success;
    public Result result;
}
