package ca.montreal.api.transport.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ca.montreal.api.transport.entities.RecordData;
import ca.montreal.api.transport.services.RequestTrafficBody;
import ca.montreal.api.transport.services.TrafficService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/traffic/v1")
@Log4j2
public class TrafficController {

	private TrafficService trafficService;

	@Autowired
	public TrafficController(final TrafficService trafficService) {
		this.trafficService = trafficService;
	}

	@Operation(summary = "Retrieve Data Traffic intersections")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "List the data traffic", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = RequestTrafficBody.class)) }) })
	@GetMapping("/show")
	@CircuitBreaker(name = "trafficService", fallbackMethod = "fallback")
	@RateLimiter(name = "trafficService", fallbackMethod = "fallback")
	@ResponseBody
	List<RecordData> getTraffic(@Valid @RequestBody RequestTrafficBody traffic) {
		return this.trafficService.getTraffic(traffic);
	}
	
	public Mono<ResponseEntity<Boolean>> fallback(Exception ex) {
	    return Mono.just(new ResponseEntity<Boolean>( Boolean.FALSE, HttpStatus.INTERNAL_SERVER_ERROR))
	        .doOnNext(result -> log.warn("fallback executed"));
	}

}
