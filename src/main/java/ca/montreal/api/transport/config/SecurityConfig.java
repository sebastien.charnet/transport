package ca.montreal.api.transport.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


	    public SecurityConfig() {
	    }

	    // Details omitted for brevity

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        // Enable CORS and disable CSRF
	        http = http.cors().and().csrf().disable();

	        // Set session management to stateless
	        http = http
	            .sessionManagement()
	            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	            .and();

	        // Set unauthorized requests exception handler
	        http = http
	            .exceptionHandling()
	            .authenticationEntryPoint(
	                (request, response, ex) -> {
	                    response.sendError(
	                        HttpServletResponse.SC_UNAUTHORIZED,
	                        ex.getMessage()
	                    );
	                }
	            )
	            .and();

	        // Set permissions on endpoints
	        http.authorizeRequests()
	            // Our public endpoints
	            .antMatchers(HttpMethod.GET, "/api/traffic/v1/**").permitAll()
	            .antMatchers(HttpMethod.GET, "/actuator/**").permitAll()
	            // Our private endpoints
	            .anyRequest().authenticated();

	    }

	    // Used by spring security if CORS is enabled.
	    @Bean
	    public CorsFilter corsFilter() {
	        UrlBasedCorsConfigurationSource source =
	            new UrlBasedCorsConfigurationSource();
	        CorsConfiguration config = new CorsConfiguration();
	        config.setAllowCredentials(true);
	        config.addAllowedOrigin("*");
	        config.addAllowedHeader("*");
	        config.addAllowedMethod("*");
	        source.registerCorsConfiguration("/**", config);
	        return new CorsFilter(source);
	    }
}
