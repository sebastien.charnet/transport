package ca.montreal.api.transport.services;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ca.montreal.api.transport.entities.RecordData;
import ca.montreal.api.transport.entities.Root;


@Service
public class TrafficServiceImpl implements TrafficService {

    @Value("${traffic.service.endpoint}")
    private String trafficEndpoint;
    
	@Override
	public List<RecordData>  getTraffic(RequestTrafficBody traffic) {
		String url = trafficEndpoint
				+ " WHERE 1 = 1 ";

		String clause = "";
		if(StringUtils.isNotEmpty(traffic.getCode_Banque())) {
			clause += "AND \"Code_Banque\"=" + traffic.getCode_Banque();
		}
		String order = " ORDER BY \"Date\" DESC";
		RestTemplate restTemplate = new RestTemplate();
		Root truck = restTemplate.getForObject(url + clause + order, Root.class);
		return truck.getResult().getRecords();
	}

}
