package ca.montreal.api.transport.services;

import java.util.List;

import ca.montreal.api.transport.entities.RecordData;

public interface TrafficService {
	List<RecordData> getTraffic(RequestTrafficBody traffic);
}