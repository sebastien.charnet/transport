package ca.montreal.api.transport.services;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ca.montreal.api.transport.entities.Root;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class ResponsePojo {
   private List<Root> roots; 
}
