package ca.montreal.api.transport.services;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RequestTrafficBody {
    @NotBlank @JsonProperty("Code_Banque") 
    public String code_Banque;
}
